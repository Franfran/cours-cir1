<html>
	<head>
		<title>Calendrier</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="CSS/styleCalendrier.css" rel="stylesheet" type="text/css" />
	</head>

	<?php
		$list_fer=array(7); //Liste pour les jours ferié


		$list_spe=array('1986-10-31','2009-4-12','2009-9-23'); //Mettez vos dates des evenements ; NB format(annee-m-j)

		$lien_redir="date_info.php"; //Lien de redirection apres un clic sur une date, NB la date selectionner va etre ajouter à ce lien afin de la récuperer ultérieurement

		$clic=1; // 1 ==> Activer les clic sur tous les dates;
						 // 2 ==> Activer les clic uniquement sur les dates speciaux;
						 // 3 ==> Désactiver les clics sur tous les dates

		$col1="#BDBDBD"; //couleur dates normales
		$col2="#8af5b5"; //couleur dates speciaux
		$col3="#BDBDBD"; //couleur dates férié

		$mois_fr = Array("", "January", "February", "March", "April", "May", "June", "July", "August","September", "October", "November", "December");

		if(isset($_GET['mois']) && isset($_GET['annee']))
		{
			$mois=$_GET['mois'];
			$annee=$_GET['annee'];
		}
		else
		{
			$mois=date("n");
			$annee=date("Y");
		}

		$ccl2=array($col1,$col2,$col3);
		$l_day=date("t",mktime(0,0,0,$mois,1,$annee));
		$x=date("N", mktime(0, 0, 0, $mois,1 , $annee));
		$y=date("N", mktime(0, 0, 0, $mois,$l_day , $annee));
		$titre=$mois_fr[$mois]."  ".$annee;
	?>

	<body>
		<center>
			<form name="dt" method="get" action="">
				<select name="mois" id="mois" onChange="change()" class="liste">
					<?php
					 echo '<a class="btn btn-primary btn-lg flex-row " href="./calendar.php?month='.($_GET['month'] - 1).'">Previous<a/>';
				 	 echo '<a class="btn btn-primary btn-lg flex-row " href="./calendar.php?month='.($_GET['month'] + 1).'">Next<a/>';
		 	 		?>

					<?php
						for($i=1;$i<13;$i++)
						{
							echo '<option value="'.$i.'"';
							if($i==$mois)
								echo ' selected ';
							echo '>'.$mois_fr[$i].'</option>';
						}
					?>
				</select>

				<select name="annee" id="annee" onChange="change()" class="liste">
					<?php
						for($i=1950;$i<2035;$i++)
						{
							echo '<option value="'.$i.'"';
							if($i==$annee)
								echo ' selected ';
							echo '>'.$i.'</option>';
						}
					?>
				</select>
			</form>

			<table class="tableau">
				<caption><?php echo $titre ;?></caption>
				<tr><th>Mon</th><th>Thu</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr>

				<tr>
					<?php
						$case=0;
						if($x>1)
							for($i=1;$i<$x;$i++)
							{
								echo '<td class="desactive">&nbsp;</td>';
								$case++;
							}
						for($i=1;$i<($l_day+1);$i++)
						{
							$f=$y=date("N", mktime(0, 0, 0, $mois,$i , $annee));
							$da=$annee."-".$mois."-".$i;
							$lien=$lien_redir;
							$lien.="?dt=".$da;
							echo "<td";
							if(in_array($da, $list_spe))
							{
								echo " class='special' onmouseover='over(this,1,2)'";
								if($clic==1||$clic==2)
									echo " onclick='go_lien(\"$lien\")' ";
							}
							else if(in_array($f, $list_fer))
							{
								echo " class='ferier' onmouseover='over(this,2,2)'";
								if($clic==1)
									echo " onclick='go_lien(\"$lien\")' ";
							}
							else
							{
								echo" onmouseover='over(this,0,2)' ";
								if($clic==1)
									echo " onclick='go_lien(\"$lien\")' ";
							}
							echo" onmouseout='over(this,0,1)'>$i</td>";
							$case++;
							if($case%7==0)
								echo "</tr><tr>";

						}
						if($y!=7)
							for($i=$y;$i<7;$i++)
							{
								echo '<td class="desactive">&nbsp;</td>';
							}
					?>
				</tr>
			</table>
		</center>
	</body>
</html>
