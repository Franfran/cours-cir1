<?php

  define('TARGET_DIRECTORY', './photos/');
  if (!empty($_FILES['photo'])){
    move_uploaded_file($_FILES['photo']['tmp_name'], TARGET_DIRECTORY . $_FILES['photo']['name']);
    $photo = TARGET_DIRECTORY . $_FILES['photo']['name'];
  }

  // Récupération des champs
  if (isset($_POST['nom']) && isset($_POST['prix']) && isset($_POST['quantite'])){
    $nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
    $prix = htmlspecialchars($_POST['prix'], ENT_QUOTES);
    $quantite = htmlspecialchars($_POST['quantite'], ENT_QUOTES);

    // Lettres comprise entre a et z, A et Z, 0 et 9 et les espaces
    if (!preg_match('/^[a-zA-Z0-9 ]+$/', $nom)){
      die('Le nom du produit n\'est pas valide, seuls les lettres et les chiffres sont acceptés.');
    }

    if ($prix < 0){
      die('Ce prix n\'est pas valide !');
    }

    $file = fopen('mes_produits.csv', 'a');

    $product = array($photo, $nom, $prix, $quantite);

    if(!fputcsv($file, $product, ';')) {
      fclose($file); die('Erreur lors de l\'enregistrement du produit');
    }

    fclose($file);

    header('Location: index.php');
    exit();
  }

?>
