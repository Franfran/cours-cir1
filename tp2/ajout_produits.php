<form method="post" action="traitement.php" enctype="multipart/form-data">

  <h1> Bienvenue sur la boutique du Runner </h1>
  <hr>

    <fieldset>
      <legend>Entrez votre produit</legend>

        <input type="file" id="file"/>
        <br>

          <div class="integer">
            <label for="nom"></label>
            <input type="text" name="nom" id="nom" placeholder="Nom" />
            <br>

            <label for="prix"></label>
            <input type="text" name="prix" id="prix" placeholder="Prix" />
            <br>

            <label for="quantite"></label>
            <input type="number" name="quantite" id="quantite" placeholder="Quantité" min="0" />
            <br>

          </div>

            <div class="envoyer">
              <input type="submit" value="Valider" id="envoyer"/>
            </div>
    </fieldset>
</form>
